RELEASE=1
VERSION=0
PKGREL=1

PACKAGE=odoo-theme-xsoftware
MODULE=theme_xsoftware

DEBSRC=${PACKAGE}_${RELEASE}.${VERSION}-${PKGREL}.debian.tar.xz
BUILDDIR?=${PACKAGE}_${RELEASE}.${VERSION}
PKGSRC=${PACKAGE}_${RELEASE}.${VERSION}.orig.tar.gz

DESTDIR=
ODOODIR=${DESTDIR}/usr/lib/python3/dist-packages/odoo
THEMEDIR=${ODOODIR}/addons/${MODULE}

GITVERSION:=$(shell git rev-parse HEAD)

ARCH=all
DEB=${PACKAGE}_${RELEASE}.${VERSION}-${PKGREL}_${ARCH}.deb


SOURCES = \
	data/theme_default_data.xml \
	__init__.py \
	__manifest__.py \
	models/__init__.py \
	models/theme_default.py \
	static/scss/main.scss \
	static/description/cover.png \
	static/description/icon.png \
	views/templates.xml

all:

.PHONY: deb
deb: ${DEB}
${DEB}:
	rm -rf ${BUILDDIR}
	rsync -a * ${BUILDDIR}
	cd ${BUILDDIR}; dpkg-buildpackage -b -us -uc
	lintian ${DEB}

install:
	install -d -m 755 ${THEMEDIR}
	install -d -m 755 ${THEMEDIR}/models
	install -d -m 755 ${THEMEDIR}/static
	install -d -m 755 ${THEMEDIR}/static/scss
	install -d -m 755 ${THEMEDIR}/static/description
	install -d -m 755 ${THEMEDIR}/views
	for i in ${SOURCES}; do install -D -m 0644 $$i ${THEMEDIR}/$$i; done

.PHONY: clean
clean:
	rm -rf *~ *.deb *.changes *.buildinfo ${BUILDDIR}

.PHONY: distclean
distclean: clean

.PHONY: dinstall
dinstall: ${DEB}
	dpkg -i ${DEB}
